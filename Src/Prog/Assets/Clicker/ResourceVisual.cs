using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceVisual : MonoBehaviour
{
    public ResourceBank ResourceBank;
    public Text Resource;
    public GameResource gameResource;


    void Update()
    {
        UpdateText(Resource, ResourceBank.GetResource(gameResource));
    }
    public void UpdateText(Text Text, int gameResource)
    {
        Text.text = gameResource.ToString();
    }
}
