using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public ResourceBank ResourceBank;
    void Start()
    {
    }

    void Update()
    {
        
    }

    public void Awake()
    {
        ResourceBank.ChangeResource(GameResource.Humans, 10);
        ResourceBank.ChangeResource(GameResource.Food, 5);
        ResourceBank.ChangeResource(GameResource.Wood, 5);
    }
}
