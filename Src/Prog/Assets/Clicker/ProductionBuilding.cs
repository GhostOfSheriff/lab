using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductionBuilding : MonoBehaviour
{

    public ResourceBank ResourceBank;
    public GameResource gameResource;
    public GameResource gameResourceLVL;
    public float ProductionTime = 2;
    private IEnumerator AddResource()
    {
        float ProductionShortTime = ProductionTime - (float)ResourceBank.GetResource(gameResourceLVL) / 10;
        float Timer = 0;
        Slider Slider = gameObject.GetComponentInChildren<Slider>();
        Debug.Log("�������: " + ResourceBank.GetResource(gameResourceLVL));
        Debug.Log("��������� �����: " + ProductionTime);
        Debug.Log("����������������� �����: " + ProductionShortTime);
        while (Timer < ProductionShortTime)
        {
            Timer+= Time.deltaTime;
            Slider.value = Timer/ProductionShortTime;
            yield return null;
        }
        Slider.value = 0;
        ResourceBank.ChangeResource(gameResource, 1);
        gameObject.GetComponent<Button>().interactable = true;
    }
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void WaitForAdd()
    {
        StartCoroutine(AddResource());
        gameObject.GetComponent<Button>().interactable = false;
    }
}
    