﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ResourceBank : MonoBehaviour
{
    private Dictionary<GameResource, int> _resources;

    //enum some 
    //{
    //    a = 5,
    //    b =78
    //}

    //private some sonetype;

    private void Awake()
    {
        //Debug.Log((int)some.b);

        _resources = new Dictionary<GameResource, int>()
        {
            [GameResource.Food] = 0,
            [GameResource.Gold] = 0,
            [GameResource.Humans] = 0,
            [GameResource.Stone] = 0,
            [GameResource.Wood] = 0,
            [GameResource.FoodProdLvl] = 1,
            [GameResource.GoldProdLvl] = 1,
            [GameResource.HumansProdLvl] = 1,
            [GameResource.StoneProdLvl] = 1,
            [GameResource.WoodProdLvl] = 1,
            [GameResource.Zero] = 0
        };
    }

    public void ChangeResource(GameResource r, int v)
    {
        if (!_resources.ContainsKey(r))
            _resources.Add(r, v);

        _resources[r] += v;
    }

    public int GetResource(GameResource r)
    {
        return _resources[r];
    }
}
