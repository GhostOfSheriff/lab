using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FormView : MonoBehaviour
{
    [SerializeField] private TMP_InputField _nickField;
    [SerializeField] private TMP_InputField _descriptionField;
    [SerializeField] private TMP_Dropdown _raceDropdown;
    [SerializeField] private TMP_Dropdown _classDropdown;
    [SerializeField] private TextMeshProUGUI _statusBarText;
    [SerializeField] private Canvas _canvas;

    private bool active = false;

    private void UpdateFields()
    {
        string nick = _nickField.text;
        
        string classText = _classDropdown
            .options[_classDropdown.value]
            .text;
        //Debug.Log("working");
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            active = !active;
            _canvas.gameObject.SetActive(active);
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter) && active == true && _nickField.text == "")
        {
            UpdateFields();
        }
        else
        {
            _statusBarText.gameObject.SetActive(true);
        }
    }
}
