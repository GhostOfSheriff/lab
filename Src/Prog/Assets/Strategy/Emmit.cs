using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : IStrategy
{
    private int _count;

    public Emmit(int count)
    {
        _count = count;
    }

    public void Perform(Transform transform) =>
        transform.GetComponent<ParticleSystem>().Emit(_count);
}
