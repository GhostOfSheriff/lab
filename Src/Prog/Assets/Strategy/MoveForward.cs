using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : IStrategy
{
    private float _speed;

    public MoveForward(float speed)
    {
        this._speed = speed;
    }

    public void Perform(Transform transform)
        => transform.position += transform.up * Time.deltaTime * _speed;
}
