using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonComponent : MonoBehaviour
{
    [SerializeField] private Performer _performer;
    [SerializeField] private Button _button;
    //������� ���� ��� ������ ���������
    public StrategyType type;
    public enum StrategyType
    {
        MoveForward,
        Rotate,
        Emmit
    }

    private void Awake()
    {
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(() =>
        {
            switch (type)
            {
                case StrategyType.MoveForward:
                    _performer.SetStrategy(new MoveForward(1));
                    break;
                case StrategyType.Rotate:
                    _performer.SetStrategy(new Rotate(1));
                    break;
                case StrategyType.Emmit:
                    _performer.SetStrategy(new Emmit(1));
                    break;
                default:
                    break;
            }
        });
    }  

    public void RunStrategy() 
    {
        //_performer.SetStrategy();
    }
}
