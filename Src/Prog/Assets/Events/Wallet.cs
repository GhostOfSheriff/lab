using UnityEngine;
using TMPro;
using UnityEngine.Events;
using System;

public class Wallet : MonoBehaviour
{
    public int coins = 10;
    [SerializeField] private TextMeshProUGUI _coinsText;
    public static event Action<int> OnCoinPickUp;
    void Start()
    {
        
    }

    void Update()
    {
        _coinsText.text = $"Coins: {coins}";
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Coin"))
        {
            coins++;
            Destroy(other.gameObject);
        }
    }
}
