using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthView : MonoBehaviour
{
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Health _health;

    private void OnEnable()
    {
        Health.OnHealthChange += UpdateHealthText;
    }

    private void OnDisable()
    {
        Health.OnHealthChange -= UpdateHealthText;
    }

    private void UpdateHealthText(int d)
    {
        _healthSlider.value = _health.HealthCount;
    }
}
