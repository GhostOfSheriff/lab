using System;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [SerializeField] private int _healthCount; 
    public int HealthCount => _healthCount;
    public static event Action<int> OnHealthChange;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            
            TakeDamage(5);
            OnHealthChange?.Invoke(5);
        }
    }

    public void TakeDamage(int d) => 
        _healthCount -= d;
}
