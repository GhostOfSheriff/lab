using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Item : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler,
    IBeginDragHandler, IDragHandler
{
    [SerializeField] private Soud _sound;

    [SerializeField] private RectTransform _rectTransform;
    private Vector2 _startPosition;

    static Transform _dragFrom;
    static Item _draggingItem;

    private Vector3 _scaleDefault = new Vector2(1, 1);
    private Vector3 _scaleChanged = new Vector2(1.5f, 1.5f);

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0) && _draggingItem != null)
        {
            Drop(_dragFrom);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _startPosition = _rectTransform.position;
        _dragFrom = transform.parent;
        _draggingItem = this;
    }

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.position = eventData.position;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _sound.PlayClickSound();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = _scaleChanged;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = _scaleDefault;
    }
    private void Drop(Transform transform)
    {
        _draggingItem.transform.SetParent(transform);
        _draggingItem.transform.localPosition = Vector2.zero;
        _dragFrom = null;
        _draggingItem = null;
    }
}